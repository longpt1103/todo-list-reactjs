import React, {Component} from 'react';
import './todo.css';
class TodoItem extends Component{
    render() {
        const item = this.props.item;
        let className = 'todo__item';
        if (item.isComplate){
            className += ' todo__item--Complate';
        }
        return (
            <div className={className}>
                <p>{this.props.item.title}</p> 
            </div>
        );
    }
}
export default TodoItem;