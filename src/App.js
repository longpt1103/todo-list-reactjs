import React from 'react';
import './App.css';

import TodoItem from './components/TodoItem' ;

function App() {
  let todoItems = [
    // {'title':'Goto School','isComplate':true},
    // {'title':'Goto Maket','isComplate':true},
    // {'title':'Goto Company','isComplate':false}
  ];
  if(todoItems.length > 0){
    return (
      <div className="App">
        {
          todoItems.map((item,index) => <TodoItem className = " " key={index} item = {item} />)
        }
      </div>
    );
  }
  else{
    return (
      <div className="App">Nothing Here</div>
    );
  }
  
}

export default App;
